<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Moemo</title>

	<!-- Fontfaces CSS-->
    <link href="{{ asset('/css/font-face.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/font-awesome-4.7/css/font-awesome.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/font-awesome-5/css/fontawesome-all.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/mdi-font/css/material-design-iconic-font.min.css') }}" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
	<link href="{{ asset('/vendor/bootstrap-4.1/bootstrap.min.css') }}" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="{{ asset('/vendor/animsition/animsition.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/wow/animate.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/css-hamburgers/hamburgers.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/slick/slick.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/select2/select2.min.css') }}" rel="stylesheet" media="all">
    <link href="{{ asset('/vendor/perfect-scrollbar/perfect-scrollbar.css') }}" rel="stylesheet" media="all">
	<link href="{{ asset('/css/lightbox.min.css') }}" rel="stylesheet" media="all">
    <!-- Main CSS-->
	<link href="{{ asset('/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">

    <link href="{{ asset('/css/theme.css') }}" rel="stylesheet" media="all">

</head>

<body class="animsition">
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
        <header class="header-desktop3 d-none d-lg-block">
            <div class="section__content section__content--p35">
                <div class="header3-wrap">
                    <div class="header__logo">
                        <a href="{{route('panel_home')}}">
                            Momeo
                        </a>
                    </div>
                    <div class="header__navbar">
                        <ul class="list-unstyled">
							<li class="has-sub">
                                <a href="{{route('panel_statistic')}}">
                                    <i class="fas fa-chart-line"></i>İstatistikler
                                    <span class="bot-line"></span>
                                </a>
                            </li>
                            <li>
                                <a href="{{route('panel_status_list')}}">
                                    <i class="fas fa-comment"></i>
                                    <span class="bot-line"></span>Durum</a>
                            </li>
                            <li>
                                <a href="{{route('panel_food_list')}}">
                                    <i class="fas fa-utensils"></i>
                                    <span class="bot-line"></span>Yemek</a>
                            </li>
                            <li class="has-sub">
                                <a href="{{route('panel_category_list')}}">
                                    <i class="fas fa-folder"></i>
                                    <span class="bot-line"></span>Kategori</a>
                            </li>
							<li class="has-sub">
                                <a href="{{route('panel_comment_list')}}">
                                    <i class="fas fa-comments"></i>
                                    <span class="bot-line"></span>Yorumlar</a>
                            </li>
							<li class="has-sub">
                                <a href="{{route('panel_order_history')}}">
                                    <i class="fas fa-history"></i>
                                    <span class="bot-line"></span>Geçmiş Siparişler</a>
                            </li>
							<li class="has-sub">
                                <a href="{{route('panel_planner')}}">
                                    <i class="far fa-calendar-alt"></i>
                                    <span class="bot-line"></span>Planlama</a>
                            </li>
                        </ul>
                    </div>
                    <div class="header__tool">
                        <div class="account-wrap">
                            <div class="account-item account-item--style2 clearfix js-item-menu">
                                <div class="content">
                                    <a class="js-acc-btn" href="#">Hesabım</a>
                                </div>
                                <div class="account-dropdown js-dropdown">
                                    <div class="account-dropdown__footer">
										<a class="dropdown-item" href="{{route('panel_hw_update')}}">
											<i class="zmdi zmdi-edit"></i>Bilgilerimi güncelle
										</a>
										<a class="dropdown-item" href="{{ route('logout') }}"
	                                       onclick="event.preventDefault();
	                                                     document.getElementById('logout-form').submit();">
	                                        <i class="zmdi zmdi-power"></i>Çıkış Yap
	                                    </a>

	                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
	                                        @csrf
	                                    </form>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- END HEADER DESKTOP-->

        <!-- HEADER MOBILE-->
        <header class="header-mobile header-mobile-2 d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid">
                    <div class="header-mobile-inner">
                        <a class="logo" href="{{route('panel_home')}}">
                            Momeo
                        </a>
                        <button class="hamburger hamburger--slider" type="button">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                    <ul class="navbar-mobile__list list-unstyled">
                        <li class="has-sub">
                            <a class="js-arrow" href="{{route('panel_home')}}">
                                <i class="fas fa-home"></i>Anasayfa</a>
                        </li>
						<li class="has-sub">
                            <a class="js-arrow" href="{{route('panel_statistic')}}">
                                <i class="fas fa-chart-line"></i>İstatistikler</a>
                        </li>

                        <li>
                            <a href="{{route('panel_status_list')}}">
                                <i class="fas fa-comment"></i>Durum</a>
                        </li>
                        <li>
                            <a href="{{route('panel_food_list')}}">
                                <i class="fas fa-utensils"></i>Yemek</a>
                        </li>
                        <li>
                            <a href="{{route('panel_category_list')}}">
                                <i class="far fa-folder"></i>Kategori</a>
                        </li>
						<li>
                            <a href="{{route('panel_comment_list')}}">
                                <i class="far fa-comments"></i>Yorumlar
							</a>
                        </li>
						<li>
							<a href="{{route('panel_order_history')}}">
								<i class="fas fa-history"></i>Geçmiş Siparişler
							</a>
						</li>
						<li>
							<a href="{{route('panel_planner')}}">
								<i class="far fa-calendar-alt"></i>Planlama
							</a>
						</li>
                    </ul>
                </div>
            </nav>
        </header>
        <div class="sub-header-mobile-2 d-block d-lg-none">
            <div class="header__tool">
                <div class="account-wrap">
                    <div class="account-item account-item--style2 clearfix js-item-menu">
                        <div class="content">
                            <a class="js-acc-btn" href="#">Hesabım</a>
                        </div>
                        <div class="account-dropdown js-dropdown">
                            <div class="account-dropdown__footer">
								<a class="dropdown-item" href="{{route('panel_hw_update')}}">
									<i class="zmdi zmdi-edit"></i>Bilgilerimi güncelle
								</a>
								<a class="dropdown-item" href="{{ route('logout') }}"
								   onclick="event.preventDefault();
												 document.getElementById('logout-form').submit();">
									<i class="zmdi zmdi-power"></i>Çıkış Yap
								</a>

								<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
									@csrf
								</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END HEADER MOBILE -->

        <!-- PAGE CONTENT-->
        <div class="page-content--bgf7">
            <!-- BREADCRUMB-->
            <section class="au-breadcrumb2">
                <div class="container">

                </div>
            </section>
            <!-- END BREADCRUMB-->

			@yield('content')

			<section class="p-t-60 p-b-20">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="copyright">
                                <p>MOEMO - 2020</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- END COPYRIGHT-->
        </div>

    </div>
	<!-- Jquery JS-->
    <script src="{{ asset('/vendor/jquery-3.2.1.min.js') }}"></script>
    <!-- Bootstrap JS-->
    <script src="{{ asset('/vendor/bootstrap-4.1/popper.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap-4.1/bootstrap.min.js') }}"></script>
    <!-- Vendor JS       -->
    <script src="{{ asset('/vendor/slick/slick.min.js') }}">
    </script>
    <script src="{{ asset('/vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('/vendor/animsition/animsition.min.js') }}"></script>
    <script src="{{ asset('/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js') }}">
    </script>
    <script src="{{ asset('/vendor/counter-up/jquery.waypoints.min.js') }}"></script>
    <script src="{{ asset('/vendor/counter-up/jquery.counterup.min.js') }}">
    </script>
    <script src="{{ asset('/vendor/circle-progress/circle-progress.min.js') }}"></script>
    <script src="{{ asset('/vendor/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('/vendor/chartjs/Chart.bundle.min.js') }}"></script>
    <script src="{{ asset('/vendor/select2/select2.min.js') }}">
    </script>
	<script src="{{ URL::asset('js/bootstrap-datetimepicker.min.js') }}"></script>

	<script src="{{ asset('/js/lightbox.min.js') }}"></script>
	<script>
	jQuery(document).ready(function($){
		$(document).on('click', '[data-toggle="lightbox"]', function(event) {

			event.preventDefault();
			$(this).ekkoLightbox();
		});
	})
	</script>
    <!-- Main JS-->
    <script src="{{ asset('/js/main.js') }}"></script>
	@yield('scripts')
</body>

</html>
<!-- end document-->
