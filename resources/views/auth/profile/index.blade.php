@extends('layouts.event')
@section('title')
	Profilim
@endsection
@section('content')

<section class="profile-info">
	<div class="container inner">
		<div class="row">
			<div class="col-md-8 left">
				<div class="avatar" style="background-image: url(/{{$user->avatar_path}});">
					<div class="overlay-a">
						<i class="fas fa-edit"></i>

					</div>

				</div>
				<div class="info-list">
					<span class="username">{{$user->name}} {{$user->surname}} <a data-toggle="modal" data-target="#update-profile" href="#"  class="btn btn-sm" style="background-color: #fe5f38;">Profil Bilgilerini Düzenle</a></span>
					<ul>
						<li><b>Kullanıcı Adı:</b> {{$user->username}}</li>
						<li><b>E-posta:</b> {{$user->email}}</li>
						<li><b>Cep Telefonu:</b> {{$user->phone}}</li>
						<li><b>Doğum Tarihi:</b> {{$user->birthday}}</li>
					</ul>
					<a href="{{route('upload_video_page')}}" class="btn btn-primary btn-md-2">Video Gönder +<div class="ripple-container"></div></a>
					<a data-toggle="modal" data-target="#promotion-codes" href="#"  class="btn btn-primary btn-md-2" style="background-color: #fe5f38;margin-left:20px;">Davetiye Kodlarım</a>
				</div>
			</div>
			<div class="col-md-4 right">
				<span class="title">Hesabım</span>
				<ul class="balance">

					<li>
						<span>Gönderilen Video:</span>
						<span>{{$tgv_count}}</span>

					</li>
					<li>
						<span>Kazanan Video:</span>
						<span>{{$kv_count}}</span>

					</li>
					<li>
						<span><b>Cüzdan:</b> {{($kv + $tgv) - $pr}} TL</span><button class="btn btn-sm payment-method" style="background-color: #0e0528;margin-bottom: 0;" data-toggle="modal" data-target="#payment-method" data-phone="{{$user->phone}}">Para Çek</button>
					</li>
				</ul>


			</div>
		</div>
		<input type="file" class="avatar-photo" accept="image/*"/>
	</div>
</section>
<section class="profile-videos">
<div class="container">
	<div class="row">
		<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="margin:0;padding: 0;">
			<div class="ui-block">
				<div class="ui-block-title">
					<div class="h6 title">
						<!--<a href="javascript:void(0);" class="active" data-class="gv">Gönderilen Videolar</a> !-->
						<a href="javascript:void(0);" class="active" data-class="bv">Beğendiğin Videolar</a></div>

				</div>
			</div>
		</div>
	</div>
</div>
<!--
<div class="container gv tab-element active">
	<div class="row">
		@foreach($g_videos as $key=>$video)
			<div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
				<div class="ui-block video-item">
					<div class="video-player">
						<img src="/img/video-bg.jpg" alt="photo">
						<a href="javascript:void(0);" class="play-button2 play-video" data-toggle="modal" data-target="#video-popup2" href="#" data-title="{{$video->name}}" data-src="{{$video->file->path}}">
							<svg class="olymp-play-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
						</a>
						<div class="overlay overlay-dark"></div>

						<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></div>
					</div>

					<div class="ui-block-content video-content video-info">
						<a href="#" class="h6 title">{{$video->name}}</a>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>
!-->
<div class="container bv tab-element active">
	<div class="row">
		@foreach($b_videos as $key=>$video)
			<div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
				<div class="ui-block video-item">
					<div class="video-player">
						<img src="{{$video->video['thumbnail']}}" alt="photo">
						<a href="javascript:void(0);" class="play-button play-video" data-toggle="modal" data-target="#video-popup" href="#" data-title="{{$video->video['title']}}" data-id="{{$video->video['video_id']}}">
							<svg class="olymp-play-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-play-icon"></use></svg>
						</a>
						<div class="overlay overlay-dark"></div>

						<div class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></div>
					</div>

					<div class="ui-block-content video-content video-info">
						<a href="#" class="h6 title">{{$video->video["title"]}}</a>
					</div>
				</div>
			</div>
		@endforeach
	</div>
</div>

</section>
<input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}" />


<div class="modal fade modal-has-swiper" id="video-popup" tabindex="-1" role="dialog" aria-labelledby="video-popup" aria-hidden="true">
	<div class="modal-dialog window-popup open-photo-popup open-photo-popup-v1" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon close-video-popup"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>

			<div class="modal-body">
				<div class="open-photo-thumb" style="padding:0!important;background-color: transparent;">
					<iframe style="width: 100%;height: 500px;" src=""></iframe>
				</div>
				<div class="modal-bottom">
					<div class="open-photo-content">
						<span class="video-title"></span>
					</div>
					<div class="share-box">
						<div class="share-btn">Yeni Sekmede Aç</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade modal-has-swiper" id="video-popup2" tabindex="-1" role="dialog" aria-labelledby="video-popup2" aria-hidden="true">
	<div class="modal-dialog window-popup open-photo-popup open-photo-popup-v1" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon close-video-popup"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>

			<div class="modal-body">
				<div class="open-photo-thumb" style="padding:0!important;background-color: transparent;">
					<video controls id="modalvideo" style="width: 100%;height: 500px;">
						<source src="" type="video/mp4">
						Your browser does not support HTML5 video.
					</video>
				</div>
				<div class="modal-bottom">
					<div class="open-photo-content" style="padding-left: 20px;">
						<span class="video-title"></span>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="modal fade" id="update-profile" tabindex="-1" role="dialog" aria-labelledby="update-profile" aria-hidden="true">
	<div class="modal-dialog window-popup create-friend-group update-profile" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>
			<div class="modal-header">
				<h6 class="title">Profil Bilgilerini Düzenle</h6>
			</div>
			<div class="modal-body">
				<div class="form-group label-floating">
                    <label class="control-label">İsim</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ $user->name }}">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Soyisim</label>
                    <input id="surname" type="text" class="form-control" name="surname" value="{{ $user->surname }}">
                </div>
				<div class="form-group label-floating">
                    <label class="control-label">Kullanıcı Adı</label>
                    <input id="username" type="text" class="form-control" name="username" value="{{ $user->username }}">
                </div>
				<div class="form-group label-floating">
                    <label class="control-label">Email Adresi</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">
                </div>
                <div class="form-group label-floating">
                    <label class="control-label">Cep Telefonu</label>
                    <input id="phone" type="text" class="form-control" name="phone" value="{{ $user->phone }}">
                </div>
				<div class="form-group label-floating">
					<label class="control-label">Doğum Tarihiniz (GG/AA/YYYY)</label>
					<input id="datetimepicker" name="birthday" value="{{$user->birthday}}" />
                </div>

				<a href="javascript:void(0);" class="btn btn-primary btn-lg full-width update-profile-info">Güncelle</a>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="promotion-codes" tabindex="-1" role="dialog" aria-labelledby="promotion-codes" aria-hidden="true">
	<div class="modal-dialog window-popup create-friend-group promotion-codes" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>
			<div class="modal-header">
				<h6 class="title">Davetiye Kodlarım</h6>
			</div>
			<div class="modal-body">
				@if(count($codes) > 0)
				<ul>
				@foreach ($codes as $key=>$code)
					<li style="font-size:20px;">{{$code->code}}</li>
				@endforeach
				</ul>
				@else
					<p>Davetiye kodunuz bulunmamaktadır.</p>
				@endif
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="payment-method" tabindex="-1" role="dialog" aria-labelledby="payment-method" aria-hidden="true">
	<div class="modal-dialog window-popup create-friend-group payment-method" role="document">
		<div class="modal-content">
			<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
				<svg class="olymp-close-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
			</a>
			<div class="modal-header">
				<h6 class="title">Ödeme Yöntemi</h6><label>Toplam Bakiyeniz: {{($kv + $tgv) - $pr}} TL</label>
			</div>
			<div class="modal-body">
				<div class="row">

					<div class="col-md-2 method" data-type="papara">
						<div class="check">
							<i class="fas fa-check"></i>
						</div>
						<img src="/img/papara.png" />
					</div>

				</div>
				<div class="row" style="margin-top: 25px;">
					<div class="col-md-12">
						<div class="form-group label-floating">
                    		<label class="control-label">Çekmek İstediğiniz Tutar</label>
                    		<input id="tutar" type="number" class="form-control" name="tutar" value="">
                		</div>
					</div>
				</div>
				<a href="javascript:void(0);" class="btn btn-primary btn-lg full-width payment-method-send">Para Çek</a>
			</div>
		</div>
	</div>
</div>

@endsection

@section('script')
<script>
	$(document).ready(function(){
		$(document).on('click','#payment-method .method',function(){
			$('#payment-method .method').removeClass('active');
			$(this).addClass('active');
		});
		$(document).on('click','.payment-method-send',function(){
			var type = $('#payment-method .method.active').data('type');
			var wallet = '{{($kv + $tgv) - $pr}}';
			var phone = '{{$user->phone}}';
			if(phone == ""){
				notify.showError('Lütfen telefon numarası giriniz.');
				return false;
			}
			if(typeof type === "undefined"){
				notify.showError('Bir ödeme yöntemi seçmeniz gerekmektedir.');
				return false;
			}
			if(parseInt($('#payment-method #tutar').val()) == ""){
				notify.showError('Lütfen tutar belirtiniz.');
				return false;
			}
			if(parseInt($('#payment-method #tutar').val()) > parseInt(wallet)){
				notify.showError('HATA! YETERSİZ BAKİYE. İşlem için yeterli bakiyeniz bulunmamaktadır.');
				return false;
			}
			if(parseInt($('#payment-method #tutar').val()) >= 25){

				if(type == "papara"){
					var phone_data = "+90{{$user->phone}}";
					connector.ajax({
						type:"GET",
						data:{phone:phone_data},
						url:"/profile/checkPapara"
					},function(response){
						if(response.Data.succeeded){
							var account_number = response.Data.data.accountNumber;
							var tutar = $('#payment-method #tutar').val();
							var user_id = "{{$user->id}}";

							connector.ajax({
								type:"POST",
								data:{ptype:type,account_number:account_number,tutar:tutar,user_id:user_id,_token:$('#csrf_token').val()},
								url:"/profile/payment-request"
							},function(response){
								if(response.Success){
									notify.showSuccess('Para çekme isteğiniz oluşturulmuştur.');
								}else{
									notify.showError('Aktif bir para çekme isteğiniz bulunmaktadır.Lütfen önceki para çekme talebinin sonuçlanmasını bekleyiniz.');
								}
							});

						}else{
							notify.showError('HATA! HESAP DOĞRULANAMADI.Üyelik ve Hesap bilgileriniz uyuşmamaktadır.');
						}
					});
				}
			}else{
				notify.showError('HATA!  GEÇERSİZ LİMİT. Minimum işlem limiti 25,00 TL');
			}

		});

		$('.profile-videos .title a').click(function(){
			var cls = $(this).data('class');
			$('.profile-videos .title a').removeClass('active');
			$(this).addClass('active');
			$('.tab-element').removeClass('active');
			$('.' + cls).addClass('active');
		});
		$(document).on('click','.play-button',function(){
			var id = $(this).data('id');
			var title = $(this).data('title');
			$('#video-popup iframe').attr('src','https://www.youtube.com/embed/' + id);
			$('#video-popup .video-title').html(title);
		});
		$(document).on('click','.play-button2',function(){
			var id = $(this).data('src');
			var title = $(this).data('title');
			var video = document.getElementById('modalvideo');
		   	video.src = "/" + id;
		   	video.play();
		    video.load();
			$('#video-popup2 .video-title').html(title);
		});
		$(document).on('click','.update-profile-info',function(){
			var post_data = {
				"name" : $('#update-profile #name').val(),
				"surname" : $('#update-profile #surname').val(),
				"username" : $('#update-profile #username').val(),
				"email" : $('#update-profile #email').val(),
				"phone" : $('#update-profile #phone').val(),
				"birthday" : $('#update-profile #datetimepicker').val(),
				"_token" : $('#csrf_token').val()
			};
			connector.ajax({
				type:"POST",
				data:post_data,
				url:"/profile/update"
			},function(response){
				if(response.Success){
					window.location.reload(false);
				}else{
					notify.showError(response.Message);
				}
			});
		});
		$('#update-profile #phone').mask('0000000000');
		$('#update-profile #datetimepicker').mask('00/00/0000');
		$(document).on('click','.overlay-a',function(){
			$('.avatar-photo').click();
		});
		$('.avatar-photo').change(function(){
			var form_data = new FormData();
			var avatar = $(this).prop('files')[0];
	    	form_data.append('avatar',avatar);
	    	form_data.append('_token',$('#csrf_token').val());
	    	connector.ajaxFormData({
				type:"POST",
				data:form_data,
				url:"/profile/avatar"
			},function(response){
				if(response.Success){
					window.location.reload(false);
				}else{
					notify.showError(response.Message);
				}
			});
		});



	});
</script>
@endsection
