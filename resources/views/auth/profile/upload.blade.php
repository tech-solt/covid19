@extends('layouts.event')

@section('title')
    Video Yükle
@endsection
@section('content')
<section class="login-page">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab">
                        @if(CheckHelper::video_count() >= 100)
                            <section class="medium-padding120">
                                <div class="container">
                                    <div class="row">
                                        <div class="col col-xl-10 m-auto col-lg-10 col-md-12 col-sm-12 col-12">
                                            <div class="page-404-content">
                                                <img src="/img/info6.png" alt="photo">
                                                <div class="crumina-module crumina-heading align-center">
                                                    <h2 class="h3 heading-title">Günlük video gönderme limitiniz dolmuştur.<br/>Duyurular için bizi takip edin.</h2>
													<ul class="socials">
														<li>
															<a href="https://www.facebook.com/G%C3%B6nder-Kazan-412723485961155/">
																<i class="fab fa-facebook-square" aria-hidden="true"></i>
															</a>
														</li>
														<li>
															<a href="https://twitter.com/GonderKazan">
																<i class="fab fa-twitter" aria-hidden="true"></i>
															</a>
														</li>
														<li>
															<a href="https://www.youtube.com/channel/UCvU3h4XDEU8iM7P8sTq25-Q">
																<i class="fab fa-youtube" aria-hidden="true"></i>
															</a>
														</li>
														<li>
															<a href="https://www.instagram.com/gonderkazanyarisma/">
																<i class="fab fa-instagram" aria-hidden="true"></i>
															</a>
														</li>
													</ul>
                                                </div>

                                                <a href="/" class="btn btn-primary btn-lg">Anasayfaya Dön</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        @else
                            <div class="title h6">Video Yükle</div>
                            <form class="content" method="post" action="{{route('upload_video_process')}}" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    @if ($message = Session::get('success'))
                                        <div class="alert alert-success alert-block">
                                            <button type="button" class="close" data-dismiss="alert">×</button>
                                            <strong>{{ $message }}</strong>
                                        </div>
                                    @endif
                                    @if (count($errors) > 0)
                                        <div class="alert alert-danger">
                                            <strong>Bir sorun oluştu!</strong> Yükleyeceğiniz videonun boyutu en fazla 100 MB olabilir.<br><br>

                                        </div>
                                    @endif
                                    <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group label-floating">
                                            <label class="control-label">Video İsmi</label>
                                            <input id="video_name" type="text" class="form-control" name="name" autofocus>
                                        </div>
                                        <div class="form-group">
                                           <textarea placeholder="Video Açıklaması" name="description" id="video_desc" class="form-control" style="height: 250px;resize: none;"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <select class="selectpicker form-control" name="game">
                                                <option value="0">Oyun Seçiniz...</option>
                                                @foreach ($games as $key=>$game)
                                                    <option value="{{$game->id}}" @if(isset($_GET["game"])&& $_GET["game"] == $game->id) selected @endif>{{$game->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
										<div class="form-group label-floating">
                                            <label class="control-label">Promosyon Kodu</label>
                                            <input id="promotion_code" type="text" class="form-control" name="promotion_code" />
                                        </div>
                                        <div class="form-group label-floating">
                                            <label>* Video yüklemeden önce lütfen <a href="javascript:void(0);"  data-toggle="modal" data-target="#term-popup">Video Teknik Şartnamesi</a>'ni okuyunuz.</label><br/>
                                            <div class="file-upload">
                                                <label for="upload" class="file-upload__label">Video Seç</label>
                                                <input id="upload" class="file-upload__input" onchange="ValidateSize(this)" type="file" name="file-upload" accept="video/mp4"><span class="selected_file_name"></span>
                                            </div>
                                        </div>


                                        <button type="submit" class="btn btn-primary btn-lg btn-send-video full-width">Yükle</button>
                                    </div>
                                </div>
                            </form>
                        @endif

                    </div>

            </div>
        </div>
    </div>
</section>
<section class="subscribe-animation scrollme bg-users">
    <a href="@if(Auth::user()) /profile @else /register @endif"><img src="/img/bottom-banner.jpg" style="width: 100%;height: auto;"/></a>
</section>
<div class="modal fade" id="term-popup" tabindex="-1" role="dialog" aria-labelledby="term-popup" aria-hidden="true">
    <div class="modal-dialog window-popup edit-my-poll-popup" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
            </a>
            <div class="modal-body">
                <div class="edit-my-poll-content">
                    <h3 style="margin-top: 0;">Video Teknik Şartnamesi</h3>
					<ul>
						<li><b>Çözünürlük:</b>  1280 x 720 px</li>
						<li><b>Kalite:</b> 720 p / 50 fps</li>
						<li><b>Süre:</b> En az 1 dakika / En fazla 3 dakika</li>
						<li><b>Boyut:</b> En fazla 100 Mb</li>
						<li><b>Ses:</b>Sadece oyun sesi. <br/>
							<p>Videonuzda sadece oyun sahnesine ait sesler olmalı. Sessiz kayıt edilen videolar ve oyun içi sesli sohbet, harici mikrofon veya müzik kaydı içeren videolar onaylanmayacaktır.</p>
						</li>
						<li>
							<b>Sahne:</b>Sadece oyun sahnesi. <br/>
							<p>Videonuz, sadece oyun sahnesi yer alacak biçimde, tam ekran (full screen) kayıt edilmelidir. Oyuncu görüntü penceresi ve oyun içi sohbet penceresi (chat panel) açık olan videolar onaylanmayacaktır. </p>
						</li>
						<li>
							<b>Montaj:</b>Yok. <br/>
							<p>Videonuz, tek bir oyun akışını içermeli. Hiçbir reklam ya da markalama yapılmamalı. Reklam içeren ve farklı kayıtların montajlanmasıyla oluşturulan videolar onaylanmayacaktır.</p>
						</li>
						<li>
							<b>Dürüstlük:</b><br/>
							<p>Oyun oynamak eğlencedir. Rekabetin eğlencemizi elimizden almasına izin vermeyelim. O sebeple; izinli ya da izinsiz, sizin oynamadığınız oyunları bizimle paylaşmayın. Videonuz daha önce hiçbir platformda yayımlanmamış olmalı. Gönderdiğiniz oyun videolarının kaydı sırasında hiçbir yardımcı yazılım ya da hile kullanılmamalıdır. </p>
						</li>
					</ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
function ValidateSize(file) {
        var FileSize = file.files[0].size / 1024 / 1024;
        if (FileSize > 100) {
        	notify.showError("Video maksimum 100 Mb olmalı. Lütfen “Video gönderme teknik şartnamesini” inceleyiniz.");
           $(file).val('');
		   $('.btn-send-video').prop('disabled', true);
		   $('.file-upload__label').html('Video Seç');
        } else {
			$('.btn-send-video').prop('disabled', false);
			$('.file-upload__label').html('Video Seçildi');
        }
    }
</script>
@endsection
