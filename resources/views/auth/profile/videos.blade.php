@extends('layouts.profile')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Videolar</div>

                    <div class="card-body">
                        
                        <ul class="list-unstyled">
                            @foreach ($videos as $key=>$video)
                                <li class="media">
                                    <img class="mr-3" src="{{$video->thumbnail}}" width="140" alt="Generic placeholder image">
                                    <div class="media-body">
                                        <a href="/{{$video->file->path}}" target="_blank"><h5 class="mt-0 mb-1">{{$video->name}}</h5></a>
                                        {{$video->description}}
                                    </div>
                                </li>
                            @endforeach 
                        </ul>
                    </div>
                    <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}" />
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

@endsection