@extends('layouts.event')
@section('title')
	Email Onayı
@endsection
@section('content')
	<section class="medium-padding120">
		<div class="container">
			<div class="row">
				<div class="col col-xl-6 m-auto col-lg-6 col-md-12 col-sm-12 col-12">
					<div class="page-404-content">
						<div class="crumina-module crumina-heading align-center">
							<h2 class="h1 heading-title">Üyeliğiniz Onaylandı!</h2>
							<p class="heading-text">E posta adresiniz onaylanmıştır.
							</p>
						</div>

						<a href="/" class="btn btn-primary btn-lg">Anasayfaya Dön</a>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection