@extends('layouts.login')
@section('title')
    Giriş Yap
@endsection
@section('content')


<div class="page-wrapper">
	<div class="page-content--bge5">
		<div class="container">
			<div class="login-wrap">
				<div class="login-content">  
					<div class="login-form">
						<form class="content" method="POST" action="{{ route('login') }}">
							 @csrf
							@if ($message = Session::get('warning'))
								<div class="alert alert-success alert-block">
									<strong>{{ $message }}</strong>
								</div>
							@endif
							<div class="form-group">
								<label>E-posta</label>
								<input id="email" type="email" class="au-input au-input--full form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
							</div>
							<div class="form-group">
								<label>Şifre</label>
								<input id="password" type="password" class="au-input au-input--full form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
							</div>

							<button class="au-btn au-btn--block au-btn--green m-b-20" type="submit">giriş yap</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>

</div>
@endsection
