@extends('layouts.admin')
@section('styles')
<style>
.basvuru_semptom{
	display: flex;
	justify-content: space-between;
	    flex-wrap: wrap;
}
.kah_risk{
	display: flex;
	flex-wrap: wrap;
}
.kah_risk .form-check{
	margin-right: 30px;
}
.lab-panel .col-md-3{
	margin-bottom: 15px;
}
.tb-form .col-md-6{
	margin-bottom: 15px;
}
.f-label{
	font-weight: 700;
}
</style>
@endsection
@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-12 m-b-35" style="display:flex;flex-direction:row;justify-content:space-between;align-items:center;">
			<h3 class="title-4">Hastalar</h3>
			<a href="{{route('covid19_add')}}" class="au-btn au-btn-icon au-btn--green"><i class="zmdi zmdi-plus"></i>hasta ekle</a>
		</div>

		<div class="col-md-12" style="margin-bottom:50px;">
			<!-- DATA TABLE -->
			<div class="table-responsive table-responsive-data2">
				<div class="form-group">
					<input type="text" id="search_user" class="form-control" placeholder="Hasta arayın..." aria-label="Username" aria-describedby="basic-addon1">
				</div>
				<table class="table table-data2">
					<thead>
						<tr>
							<th>Hasta</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
						@foreach ($hastalar as $key=>$hasta)
							<tr class="tr-shadow">
								<td>{{$hasta->ad}} {{$hasta->soyad}}</td>
								<td>
									<div class="table-data-feature">
										<a href="{{route('covid19_update',$hasta->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="Güncelle">
											<i class="zmdi zmdi-edit"></i>
										</a>
										<a href="{{route('covid19_delete_process',$hasta->id)}}" class="item" data-toggle="tooltip" data-placement="top" title="Sil">
											<i class="zmdi zmdi-delete"></i>
										</button>
									</div>
								</td>
							</tr>
							<tr class="spacer"></tr>

						@endforeach
					</tbody>
				</table>
			</div>


			<!-- END DATA TABLE -->
		</div>
		{{ $hastalar->links() }}
	</div>
</div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			language:"tr",
			format: 'dd/mm/yyyy',
		});
		$('.eh_radio').click(function(){

			var target = $(this).data('target');
			if($(this).val() == "true"){
				$('.' + target).show();
			}else{
				$('.' + target).hide();
			}
		})
  	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
<script>
	$('#search_user').typeahead({

	displayText: function(item) { return item.ad + " "+item.soyad + " " + item.tckimlik},
	source:  function (query, process) {

		return $.get("{{route('covid19_search')}}", { query: query }, function (data) {
			return process(data);
		});
	},
	updater:function (item) {
		var base = '/admin/covid19/update';
		window.location.href = base + "/" + item.id;
		return item;
	}
});
</script>
@endsection
