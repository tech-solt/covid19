@extends('layouts.admin')
@section('styles')
<style>
.basvuru_semptom{
	display: flex;
	justify-content: space-between;
	    flex-wrap: wrap;
}
.kah_risk{
	display: flex;
}
.kah_risk .form-check{
	margin-right: 30px;
}
.lab-panel .col-md-3{
	margin-bottom: 15px;
}
.tb-form .col-md-6{
	margin-bottom: 15px;
}
.f-label{
	font-weight: 700;
}
</style>
@endsection
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{$hasta->ad}} {{$hasta->soyad}}</div>

                    <div class="card-body">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">×</button>
                                <strong>{{ $message }}</strong>
                            </div>
                        @endif
                        <form method="post" action="{{route('covid19_update_process')}}" enctype="multipart/form-data">
                        <div class="form-group row">
                            <div class="col-md-6">
								<label class="f-label">Adı*</label>
                                <input id="name" type="text" placeholder="Adı" class="form-control" name="hasta_adi" value="{{$hasta->ad}}">
                            </div>
							<div class="col-md-6">
								<label class="f-label">Soyadı*</label>
                                <input id="limit" type="text" placeholder="Soyadı" class="form-control" name="hasta_soyadi" value="{{$hasta->soyad}}">
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-6">
								<label class="f-label">Yaşı*</label>
                                <input id="limit" type="text" placeholder="Yaşı" class="form-control" name="hasta_yasi" value="{{$hasta->yas}}">
                            </div>
							<div class="col-md-6">
								<label class="f-label">Cinsiyeti*</label>
								<select class="form-control" name="hasta_cinsiyeti">
									<option value="erkek" @if($hasta->cinsiyet == "erkek") selected @endif>Erkek</option>
									<option value="kadin" @if($hasta->cinsiyet == "kadin") selected @endif>Kadın</option>
								</select>
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-12">
								<label class="f-label">TC Kimlik No*</label>
                                <input id="name" type="text" placeholder="Tc Kimlik No" class="form-control" name="hasta_tckimlik" value="{{$hasta->tckimlik}}">
                            </div>
                        </div>
						<div class="form-group row">
							<div class="col-md-6">
								<label class="f-label">Başvuru Tarihi</label>
								<input type="text" class="form-control datepicker" placeholder="Başvuru Tarihi" name="hasta_basvuru_tarihi" value="{{$hasta->data['hasta_basvuru_tarihi']}}">
							</div>
							<div class="col-md-6">
								<label class="f-label">Şikayetlerinin Başlangıç Tarihi</label>
								<input type="text" class="form-control datepicker" placeholder="Şikayetlerinin Başlangıç Tarihi" name="hasta_s_baslangic_tarihi" value="{{$hasta->data['hasta_s_baslangic_tarihi']}}">
							</div>
						</div>
						<div class="form-group row">
                            <div class="col-md-12">
								<label class="f-label">Ön Tanı</label>
                                <textarea class="form-control" name="hasta_ontani">{{$hasta->data["hasta_ontani"]}}</textarea>
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-6">
								<label class="f-label">Sistolik Kb</label>
                                <input id="limit" type="text" placeholder="Sistolik Kb" class="form-control" name="hasta_sistolik" value="{{$hasta->data['hasta_sistolik']}}">
                            </div>
							<div class="col-md-6">
								<label class="f-label">Diastolik Kb</label>
                                <input id="limit" type="text" placeholder="Diastolik Kb" class="form-control" name="hasta_diastolik" value="{{$hasta->data['hasta_diastolik']}}">
                            </div>
                        </div>
						<div class="form-group row">
                            <div class="col-md-6">
								<label class="f-label">Nabız</label>
                                <input id="limit" type="text" placeholder="Nabız" class="form-control" name="hasta_nabiz" value="{{$hasta->data['hasta_nabiz']}}">
                            </div>
							<div class="col-md-6">
								<label class="f-label">SaO2</label>
                                <input id="limit" type="text" placeholder="SaO2" class="form-control" name="hasta_sa02" value="{{$hasta->data['hasta_sa02']}}">
                            </div>
                        </div>
						<label class="f-label">Başvuru Semptomları</label>
						<div class="form-group row">
							<div class="col-md-12 basvuru_semptom">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_basvuru_semptom[anjina]" id="defaultCheck1" @if(isset($hasta->data["hasta_basvuru_semptom"]["anjina"])) checked @endif>
									<label class="form-check-label" for="defaultCheck1">
										Anjina
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_basvuru_semptom[dispne]" id="defaultCheck2" @if(isset($hasta->data["hasta_basvuru_semptom"]["dispne"])) checked @endif>
									<label class="form-check-label" for="defaultCheck2">
										Dispne
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_basvuru_semptom[carpinti]" id="defaultCheck3" @if(isset($hasta->data["hasta_basvuru_semptom"]["carpinti"])) checked @endif>
									<label class="form-check-label" for="defaultCheck3">
										Çarpıntı
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_basvuru_semptom[senkop]" id="defaultCheck4" @if(isset($hasta->data["hasta_basvuru_semptom"]["senkop"])) checked @endif>
									<label class="form-check-label" for="defaultCheck4">
										Senkop
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_basvuru_semptom[kardiyakarrest]" id="defaultCheck5" @if(isset($hasta->data["hasta_basvuru_semptom"]["kardiyakarrest"])) checked @endif>
									<label class="form-check-label" for="defaultCheck5">
										Kardiyak Arrest
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_basvuru_semptom[diger]" id="defaultCheck6" @if(isset($hasta->data["hasta_basvuru_semptom"]["diger"])) checked @endif>
									<label class="form-check-label" for="defaultCheck6">
										Diğer
									</label>
								</div>
							</div>
                        </div>
						<label class="f-label">KAH risk faktörleri var mı?</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_kah_risk" type="radio" value="true" data-target="kah-form" @if($hasta->data["hasta_kah_risk"] == "true") checked @endif>
									<label class="form-check-label">
										Evet
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_kah_risk" type="radio" value="false" data-target="kah-form" @if($hasta->data["hasta_kah_risk"] == "false") checked @endif>
									<label class="form-check-label">
										Hayır
									</label>
								</div>
							</div>
							<div class="col-md-12 kah-form kah_risk" style=" @if($hasta->data['hasta_kah_risk'] =='false') display:none; @endif margin-top:20px;">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kah[dm]" id="kah1" @if(isset($hasta->data['hasta_kah']['dm'])) checked @endif>
									<label class="form-check-label" for="kah1">
										DM
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kah[ht]" id="kah2" @if(isset($hasta->data['hasta_kah']['ht'])) checked @endif>
									<label class="form-check-label" for="kah2">
										HT
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kah[mi]" id="kah3" @if(isset($hasta->data['hasta_kah']['mi'])) checked @endif>
									<label class="form-check-label" for="kah3">
										Ailede erken yaş MI öyküsü
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kah[hl]" id="kah4" @if(isset($hasta->data['hasta_kah']['hl'])) checked @endif>
									<label class="form-check-label" for="kah4">
										HL
									</label>
								</div>
							</div>
						</div>

						<label class="f-label">KV hastalık öyküsü var mı?</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_kv_oykusu" type="radio" value="true" data-target="kv-form" @if($hasta->data["hasta_kv_oykusu"] == "true") checked @endif>
									<label class="form-check-label">
										Evet
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_kv_oykusu" type="radio" value="false" data-target="kv-form" @if($hasta->data["hasta_kv_oykusu"] == "false") checked @endif>
									<label class="form-check-label">
										Hayır
									</label>
								</div>
							</div>
							<div class="col-md-12 kv-form kah_risk" style="@if($hasta->data['hasta_kv_oykusu'] =='false') display:none; @endif margin-top:20px;">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kv[mi]" id="kv1" @if(isset($hasta->data['hasta_kv']['mi'])) checked @endif>
									<label class="form-check-label" for="kv1">
										MI
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kv[kky]" id="kv2" @if(isset($hasta->data['hasta_kv']['kky'])) checked @endif>
									<label class="form-check-label" for="kv2">
										KKY
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kv[cabg]" id="kv3" @if(isset($hasta->data['hasta_kv']['cabg'])) checked @endif>
									<label class="form-check-label" for="kv3">
										CABG
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kv[pci]" id="kv4" @if(isset($hasta->data['hasta_kv']['pci'])) checked @endif>
									<label class="form-check-label" for="kv4">
										PCI
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_kv[kapak_cerrahisi]" id="kv5" @if(isset($hasta->data['hasta_kv']['kapak_cerrahisi'])) checked @endif>
									<label class="form-check-label" for="kv5">
										Kapak Cerrahisi
									</label>
								</div>
							</div>
						</div>


						<label class="f-label">Eşlik eden hastalık var mı?</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_eslik_hastalik" type="radio" value="true" data-target="es-form" @if($hasta->data["hasta_eslik_hastalik"] == "true") checked @endif>
									<label class="form-check-label">
										Evet
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_eslik_hastalik" type="radio" value="false" data-target="es-form" @if($hasta->data["hasta_eslik_hastalik"] == "false") checked @endif>
									<label class="form-check-label">
										Hayır
									</label>
								</div>
							</div>
							<div class="col-md-12 es-form kah_risk" style="@if($hasta->data['hasta_eslik_hastalik'] =='false') display:none; @endif margin-top:20px;">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_eslik[malignite]" id="es1" @if(isset($hasta->data['hasta_eslik']['malignite'])) checked @endif>
									<label class="form-check-label" for="es1">
										Malignite
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_eslik[koah]" id="es2" @if(isset($hasta->data['hasta_eslik']['koah'])) checked @endif>
									<label class="form-check-label" for="es2">
										KOAH
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_eslik[bobrek]" id="es3" @if(isset($hasta->data['hasta_eslik']['bobrek'])) checked @endif>
									<label class="form-check-label" for="es3">
										Böbrek Yetmezliği
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_eslik[kanama]" id="es4" @if(isset($hasta->data['hasta_eslik']['kanama'])) checked @endif>
									<label class="form-check-label" for="es4">
										Kanama Öyküsü
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_eslik[trombofili]" id="es5" @if(isset($hasta->data['hasta_eslik']['trombofili'])) checked @endif>
									<label class="form-check-label" for="es5">
										Trombofili Öyküsü
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label class="f-label">EKG Görseli</label><br/>
								<input type="file" name="hasta_ekg" id="file-to-upload" accept="image/*" />
								<input type="hidden" name="ekg_file" value="{{$hasta->data['hasta_ekg']}}"/><br/><br/>
								@if($ekg_file)
									<a href="/{{$ekg_file->path}}"  data-lightbox="image-{{$ekg_file->id}}" data-title="">Görseli Gör</a>
								@endif
							</div>
						</div>

						<label class="f-label">Özellik yok(Normal sınırlarda)</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_ozellik_yok" type="radio" value="true" @if($hasta->data["hasta_ozellik_yok"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_ozellik_yok" type="radio" value="false" @if($hasta->data["hasta_ozellik_yok"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>
						<label class="f-label">Yeni gelişen LBBB</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_lbbb" type="radio" value="true" @if($hasta->data["hasta_lbbb"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_lbbb" type="radio" value="false" @if($hasta->data["hasta_lbbb"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>
						<label class="f-label">RBBB gelişimi</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_rbbb" type="radio" value="true"  @if($hasta->data["hasta_rbbb"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_rbbb" type="radio" value="false"  @if($hasta->data["hasta_rbbb"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>
						<label class="f-label">Ritim</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_ritim[sinuzal]" id="hr3"  @if(isset($hasta->data["hasta_ritim"]["sinuzal"])) checked @endif>
									<label class="form-check-label" for="hr3">
										Sinuzal
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_ritim[flutter]" id="hr4" @if(isset($hasta->data["hasta_ritim"]["flutter"])) checked @endif>
									<label class="form-check-label" for="hr4">
										AF/A Flutter
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_ritim[pace_ritm]" id="hr5" @if(isset($hasta->data["hasta_ritim"]["pace_ritm"])) checked @endif>
									<label class="form-check-label" for="hr5">
										Pace Ritm
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_ritim[vtvf]" id="hr5" @if(isset($hasta->data["hasta_ritim"]["vtvf"])) checked @endif>
									<label class="form-check-label" for="hr5">
										VT/VF
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_ritim[diger]" id="hr5" @if(isset($hasta->data["hasta_ritim"]["diger"])) checked @endif>
									<label class="form-check-label" for="hr5">
										Diğer
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label class="f-label">Hız</label>
								<input id="name" type="text" placeholder="Hız" class="form-control" name="hasta_ritimhiz" value="{{$hasta->data['hasta_ritimhiz']}}">
							</div>
						</div>
						<label class="f-label">AV Blok</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_av" type="radio" value="true" @if($hasta->data["hasta_av"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_av" type="radio" value="false" @if($hasta->data["hasta_av"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>
						<label class="f-label">Ardışık iki derivasyonda ST segment elevasyonu≥1mm</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_ardisik" type="radio" value="true" data-target="ar-form" @if($hasta->data["hasta_ardisik"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_ardisik" type="radio" value="false" data-target="ar-form" @if($hasta->data["hasta_ardisik"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
							<div class="row ar-form" style="@if($hasta->data['hasta_ardisik'] == 'false') display:none; @endif margin-top:20px;margin-left:0;margin-right:0;">
								<div class="col-md-12 kah_risk">
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[di]" id="ha1" @if(isset($hasta->data["hasta_ardisiks"]["di"])) checked @endif>
										<label class="form-check-label" for="ha1">
											DI
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[dii]" id="ha2" @if(isset($hasta->data["hasta_ardisiks"]["dii"])) checked @endif>
										<label class="form-check-label" for="ha2">
											DII
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[diii]" id="ha3" @if(isset($hasta->data["hasta_ardisiks"]["diii"])) checked @endif>
										<label class="form-check-label" for="ha3">
											DIII
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[avr]" id="ha4" @if(isset($hasta->data["hasta_ardisiks"]["avr"])) checked @endif>
										<label class="form-check-label" for="ha4">
											aVR
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[avl]" id="ha5" @if(isset($hasta->data["hasta_ardisiks"]["avl"])) checked @endif>
										<label class="form-check-label" for="ha5">
											aVL
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[avf]" id="ha6" @if(isset($hasta->data["hasta_ardisiks"]["avf"])) checked @endif>
										<label class="form-check-label" for="ha6">
											aVF
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[vi]" id="ha7" @if(isset($hasta->data["hasta_ardisiks"]["vi"])) checked @endif>
										<label class="form-check-label" for="ha7">
											VI
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[vii]" id="ha8" @if(isset($hasta->data["hasta_ardisiks"]["vii"])) checked @endif>
										<label class="form-check-label" for="ha8">
											VII
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[viii]" id="ha9" @if(isset($hasta->data["hasta_ardisiks"]["viii"])) checked @endif>
										<label class="form-check-label" for="ha9">
											VIII
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[v4]" id="ha10" @if(isset($hasta->data["hasta_ardisiks"]["v4"])) checked @endif>
										<label class="form-check-label" for="ha10">
											V4
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[v5]" id="ha11" @if(isset($hasta->data["hasta_ardisiks"]["v5"])) checked @endif>
										<label class="form-check-label" for="ha11">
											V5
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_ardisiks[v6]" id="ha12" @if(isset($hasta->data["hasta_ardisiks"]["v6"])) checked @endif>
										<label class="form-check-label" for="ha12">
											V6
										</label>
									</div>
								</div>
							</div>
						</div>

						<label class="f-label">Ardışık iki derivasyonda ST segment depresyonu≥1mm</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_ardisikd" type="radio" value="true" @if($hasta->data["hasta_ardisikd"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_ardisikd" type="radio" value="false" @if($hasta->data["hasta_ardisikd"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>
						<label class="f-label">T dalga inversiyonu</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_tdalga" type="radio" value="true" @if($hasta->data["hasta_tdalga"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_tdalga" type="radio" value="false" @if($hasta->data["hasta_tdalga"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>
						<label class="f-label">Non-spesifik ST/T değişiklikleri</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input" name="hasta_nonsp" type="radio" value="true" @if($hasta->data["hasta_nonsp"] == "true") checked @endif>
									<label class="form-check-label">
										Var
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" name="hasta_nonsp" type="radio" value="false" @if($hasta->data["hasta_nonsp"] == "false") checked @endif>
									<label class="form-check-label">
										Yok
									</label>
								</div>
							</div>
						</div>


						<label class="f-label">Laboratuvar Paneli</label>
						<div class="form-group row lab-panel">
							<div class="col-md-3">
								<label class="f-label">Trop 1</label>
                                <input id="limit" type="text" placeholder="Trop 1" class="form-control" name="hasta_lab_trop1" value={{$hasta->data['hasta_lab_trop1']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Trop 2</label>
                                <input id="limit" type="text" placeholder="Trop 2" class="form-control" name="hasta_lab_trop2" value={{$hasta->data['hasta_lab_trop2']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Trop 3</label>
                                <input id="limit" type="text" placeholder="Trop 3" class="form-control" name="hasta_lab_trop3" value={{$hasta->data['hasta_lab_trop3']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">WBC</label>
                                <input id="limit" type="text" placeholder="WBC" class="form-control" name="hasta_lab_wbc" value={{$hasta->data['hasta_lab_wbc']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">HB</label>
                                <input id="limit" type="text" placeholder="HB" class="form-control" name="hasta_lab_hb" value={{$hasta->data['hasta_lab_hb']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Trombosit</label>
                                <input id="limit" type="text" placeholder="Trombosit" class="form-control" name="hasta_lab_trombosit" value={{$hasta->data['hasta_lab_trombosit']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Ph</label>
                                <input id="limit" type="text" placeholder="Ph" class="form-control" name="hasta_lab_ph" value={{$hasta->data['hasta_lab_ph']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">D-Dimer</label>
                                <input id="limit" type="text" placeholder="D-Dimer" class="form-control" name="hasta_lab_ddimer" value={{$hasta->data['hasta_lab_ddimer']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Lenfosit</label>
                                <input id="limit" type="text" placeholder="Lenfosit" class="form-control" name="hasta_lab_lenfosit" value={{$hasta->data['hasta_lab_lenfosit']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Lökosit</label>
                                <input id="limit" type="text" placeholder="Lökosit" class="form-control" name="hasta_lab_lokosit" value={{$hasta->data['hasta_lab_lokosit']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Ferritin</label>
                                <input id="limit" type="text" placeholder="Ferritin" class="form-control" name="hasta_lab_ferritin" value={{$hasta->data['hasta_lab_ferritin']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">CRP</label>
                                <input id="limit" type="text" placeholder="CRP" class="form-control" name="hasta_lab_crp" value={{$hasta->data['hasta_lab_crp']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Pro BNP</label>
                                <input id="limit" type="text" placeholder="Pro BNP" class="form-control" name="hasta_lab_probnp" value={{$hasta->data['hasta_lab_probnp']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">BNP</label>
                                <input id="limit" type="text" placeholder="BNP" class="form-control" name="hasta_lab_bnp" value={{$hasta->data['hasta_lab_bnp']}}>
                            </div>
							<div class="col-md-3">
								<label class="f-label">Sedim</label>
                                <input id="limit" type="text" placeholder="Sedim" class="form-control" name="hasta_lab_sedim" value={{$hasta->data['hasta_lab_sedim']}}>
                            </div>
                        </div>
						<label class="f-label">Co-vid tanı test sonucu</label>
						<div class="form-group row">
                            <div class="col-md-3">
								<label class="f-label">PCR Pozitif</label>
                                <input id="name" type="text" placeholder="PCR Pozitif" class="form-control" name="hasta_covid_pcr_pozitif" value="{{$hasta->data['hasta_covid_pcr_pozitif']}}">
                            </div>
							<div class="col-md-3">
								<label class="f-label">PCR Negatif</label>
                                <input id="name" type="text" placeholder="PCR Negatif" class="form-control" name="hasta_covid_pcr_negatif" value="{{$hasta->data['hasta_covid_pcr_negatif']}}">
                            </div>
							<div class="col-md-3">
								<label class="f-label">İmunolojik Pozitif</label>
                                <input id="limit" type="text" placeholder="İmunolojik Pozitif" class="form-control" name="hasta_covid_imunolojik_pozitif" value="{{$hasta->data['hasta_covid_imunolojik_pozitif']}}">
                            </div>
							<div class="col-md-3">
								<label class="f-label">İmunolojik Negatif</label>
                                <input id="limit" type="text" placeholder="İmunolojik Negatif" class="form-control" name="hasta_covid_imunolojik_negatif" value="{{$hasta->data['hasta_covid_imunolojik_negatif']}}">
                            </div>
                        </div>

						<label class="f-label">Hasta acil KAG alındı mı?</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_acil_kag" type="radio" value="true" data-target="kag-form" @if($hasta->data["hasta_acil_kag"] == "true") checked @endif>
									<label class="form-check-label">
										Evet
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_acil_kag" type="radio" value="false" data-target="kag-form" @if($hasta->data["hasta_acil_kag"] == "false") checked @endif>
									<label class="form-check-label">
										Hayır
									</label>
								</div>
							</div>
							<div class="col-md-12 kag-form" style="@if($hasta->data['hasta_acil_kag'] =='false') display:none; @endif margin-top:20px;">
								<label class="f-label">Kag Raporu Görseli</label><br/>
								<input type="file" name="hasta_kag_raporu" id="file-to-upload" accept="image/*" />
								<input type="hidden" name="kag_file" value="{{$hasta->data['hasta_kag_raporu']}}"/>
								<br/><br/>
								@if($kag_file)
									<a href="/{{$kag_file->path}}"  data-lightbox="image-{{$kag_file->id}}" data-title="">Görseli Gör</a>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label class="f-label">BT Görseli</label><br/>
								<input type="file" name="hasta_bt" id="file-to-upload" accept="image/*" />
								<input type="hidden" name="bt_file" value="{{$hasta->data['hasta_bt']}}"/>
								<br/><br/>
								@if($hasta_bt)
									<a href="/{{$hasta_bt->path}}"  data-lightbox="image-{{$hasta_bt->id}}" data-title="">Görseli Gör</a>
								@endif
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<label class="f-label">EKO Görseli</label><br/>
								<input type="file" name="hasta_eko" id="file-to-upload" accept="image/*" />
								<input type="hidden" name="eko_file" value="{{$hasta->data['hasta_eko']}}"/>
								<br/><br/>
								@if($hasta_eko)
									<a href="/{{$hasta_eko->path}}"  data-lightbox="image-{{$hasta_eko->id}}" data-title="">Görseli Gör</a>
								@endif
							</div>
						</div>
						<label class="f-label">Ekokardiyografi</label>
						<div class="form-group row">
                            <div class="col-md-6">
								<label class="f-label">EF</label>
                                <input id="name" type="text" placeholder="EF" class="form-control" name="hasta_eko_ef"  value="{{$hasta->data['hasta_eko_ef']}}">
                            </div>
							<div class="col-md-6">
								<label class="f-label">Duvar Hareket Kusuru</label>

								<div class="col-md-6">
									<div class="kah_risk">
										<div class="form-check">
											<input class="form-check-input" name="hasta_eko_duvar" type="radio" value="true" @if($hasta->data['hasta_eko_duvar'] == "true") checked @endif>
											<label class="form-check-label">
												Evet
											</label>
										</div>
										<div class="form-check">
											<input class="form-check-input" name="hasta_eko_duvar" type="radio" value="false" @if($hasta->data['hasta_eko_duvar'] == "false") checked @endif>
											<label class="form-check-label">
												Hayır
											</label>
										</div>
									</div>
	                            </div>
                            </div>
                        </div>

						<label class="f-label">Trombolitik yapıldı mı?</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_trombolitik" type="radio" value="true" data-target="tb-form" @if($hasta->data["hasta_trombolitik"] == "true") checked @endif>
									<label class="form-check-label">
										Evet
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_trombolitik" type="radio" value="false" data-target="tb-form" @if($hasta->data["hasta_trombolitik"] == "false") checked @endif>
									<label class="form-check-label">
										Hayır
									</label>
								</div>
							</div>
							<div class="row tb-form" style="@if($hasta->data['hasta_trombolitik'] =='false') display:none; @endif margin-top:20px;margin-left:0;margin-right:0;">
								<div class="kah_risk">
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_trom[1]" id="ht1" @if(isset($hasta->data["hasta_trom"][1])) checked @endif>
										<label class="form-check-label" for="ht1">
											KY Bulguları Kıllip Klas 1
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_trom[2]" id="ht2" @if(isset($hasta->data["hasta_trom"][2])) checked @endif>
										<label class="form-check-label" for="ht2">
											KY Bulguları Kıllip Klas 2
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_trom[3]" id="ht3" @if(isset($hasta->data["hasta_trom"][3])) checked @endif>
										<label class="form-check-label" for="ht3">
											KY Bulguları Kıllip Klas 3
										</label>
									</div>
									<div class="form-check">
										<input class="form-check-input" type="checkbox" value="true" name="hasta_trom[4]" id="ht4" @if(isset($hasta->data["hasta_trom"][4])) checked @endif>
										<label class="form-check-label" for="ht4">
											KY Bulguları Kıllip Klas 4
										</label>
									</div>
								</div>
							</div>
						</div>


						<label class="f-label">Özel COVID-19 tedavisi alıyor mu?</label>
						<div class="form-group row">
							<div class="col-md-12 kah_risk">
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_covid_ted" type="radio" value="true" data-target="cv-form" @if($hasta->data["hasta_covid_ted"] == "true") checked @endif>
									<label class="form-check-label">
										Evet
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input eh_radio" name="hasta_covid_ted" type="radio" value="false" data-target="cv-form" @if($hasta->data["hasta_covid_ted"] == "false") checked @endif>
									<label class="form-check-label">
										Hayır
									</label>
								</div>
							</div>
							<div class="col-md-12 cv-form kah_risk" style="@if($hasta->data['hasta_acil_kag'] =='false') display:none; @endif margin-top:20px;">
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_covid_tedavisi[klorokin]" id="cv1" @if(isset($hasta->data['hasta_covid_tedavisi']['klorokin'])) checked @endif>
									<label class="form-check-label" for="cv1">
										Klorokin
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_covid_tedavisi[azitromisin]" id="cv2" @if(isset($hasta->data['hasta_covid_tedavisi']['azitromisin'])) checked @endif>
									<label class="form-check-label" for="cv2">
										Azitromisin
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_covid_tedavisi[antiviral]" id="cv3" @if(isset($hasta->data['hasta_covid_tedavisi']['antiviral'])) checked @endif>
									<label class="form-check-label" for="cv3">
										Antiviral
									</label>
								</div>
								<div class="form-check">
									<input class="form-check-input" type="checkbox" value="true" name="hasta_covid_tedavisi[diger]" id="cv4" @if(isset($hasta->data['hasta_covid_tedavisi']['diger'])) checked @endif>
									<label class="form-check-label" for="cv4">
										Diğer
									</label>
								</div>

							</div>
						</div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary" id="send_video_btn">
                                    {{ __('Gönder') }}
                                </button>
                            </div>
                        </div>
                        <input type="hidden" name="_token" id="csrf_token" value="{{csrf_token()}}" />
						<input type="hidden" name="id" id="id" value="{{$hasta->id}}" />
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script type="text/javascript">
	$(function() {
		$('.datepicker').datepicker({
			language:"tr",
			format: 'dd/mm/yyyy',
		});
		$('.eh_radio').click(function(){

			var target = $(this).data('target');
			if($(this).val() == "true"){
				$('.' + target).show();
			}else{
				$('.' + target).hide();
				$.each($('.' + target).find('input[type="checkbox"]'),function(i,v){
					$(v).attr('checked',false);
				})

				$.each($('.' + target).find('input[type="text"]'),function(i,v){
					$(v).val('');
				})
			}
		})
  	});
</script>
@endsection
