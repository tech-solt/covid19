<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Hasta extends Model
{
    protected $table = 'hasta';
    protected $fillable = [
        'ad',
        'soyad',
		'yas',
		'cinsiyet',
		'tckimlik',
		'data',
		'user'
    ];
	protected $casts = [
        'data' => 'array'
    ];
}
