<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Hasta;
use App\File;
use App\Helpers\FileHelper;
class Covid19Controller extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function list(Request $request)
    {
		if($request->user()->email == "dreyupozkan@gmail.com"){
			$hasta = Hasta::paginate(10);
		}else{
			$hasta = Hasta::where('user',$request->user()->id)->paginate(10);
		}

        return view('covid19.list',[
			"hastalar" => $hasta
		]);
    }
	public function add()
    {
        return view('covid19.add');
    }
	public function add_process(Request $request){
		$data=array();
		$data["ad"] = $request->hasta_adi;
		$data["soyad"] = $request->hasta_soyadi;
		$data["yas"] = $request->hasta_yasi;
		$data["cinsiyet"] = $request->hasta_cinsiyeti;
		$data["tckimlik"] = $request->hasta_tckimlik;
		$data["data"] = $request->all();
		if(isset($data["data"]["hasta_acil_kag"])){
			if($data["data"]["hasta_acil_kag"] == "true"){
				$kag = FileHelper::add_file($request->hasta_kag_raporu,'assets/files/hasta/')->id;
			}else{
				$kag = 0;
			}
		}else{
			$kag = 0;
		}
		if($request->hasta_ekg){
			$ekg = FileHelper::add_file($request->hasta_ekg,'assets/files/hasta/')->id;
		}else{
			$ekg  = 0;
		}
		if($request->hasta_bt){
			$bt = FileHelper::add_file($request->hasta_bt,'assets/files/hasta/')->id;
		}else{
			$bt  = 0;
		}
		if($request->hasta_eko){
			$eko = FileHelper::add_file($request->hasta_eko,'assets/files/hasta/')->id;
		}else{
			$eko = 0;
		}
		$data["data"]["hasta_kag_raporu"] = $kag;
		$data["data"]["hasta_ekg"] = $ekg;
		$data["data"]["hasta_bt"] = $bt;
		$data["data"]["hasta_eko"] = $eko;
		$data["user"] = $request->user()->id;
		Hasta::create($data);

		return back()->with('success','Hasta başarıyla eklendi!');
	}

	public function update($id)
    {
		$hasta = Hasta::find($id);
        return view('covid19.update',[
			"hasta" => $hasta,
			"ekg_file" => File::find($hasta->data['hasta_ekg']),
			"kag_file" => File::find($hasta->data['hasta_kag_raporu']),
			"hasta_bt" => File::find($hasta->data['hasta_bt']),
			"hasta_eko" => File::find($hasta->data['hasta_eko'])
		]);
    }
	public function update_process(Request $request){
		$data=array();
		$data["data"] = $request->all();
		$hasta = Hasta::find($request->id);
		$hasta->ad = $request->hasta_adi;
		$hasta->soyad = $request->hasta_soyadi;
		$hasta->yas = $request->hasta_yasi;
		$hasta->cinsiyet = $request->hasta_cinsiyeti;
		$hasta->tckimlik = $request->hasta_tckimlik;
		if($request->hasta_kag_raporu){
			$kag = FileHelper::add_file($request->hasta_kag_raporu,'assets/files/hasta/')->id;
			$data["data"]["hasta_kag_raporu"] = $kag;
		}else{
			$data["data"]["hasta_kag_raporu"] = $request->kag_file;
		}
		if($request->hasta_ekg){
			$ekg = FileHelper::add_file($request->hasta_ekg,'assets/files/hasta/')->id;
			$data["data"]["hasta_ekg"] = $ekg;
		}else{
			$data["data"]["hasta_ekg"] = $request->ekg_file;
		}

		if($request->hasta_bt){
			$bt = FileHelper::add_file($request->hasta_bt,'assets/files/hasta/')->id;
			$data["data"]["hasta_bt"] = $bt;
		}else{
			$data["data"]["hasta_bt"] = $request->bt_file;
		}

		if($request->hasta_eko){
			$eko = FileHelper::add_file($request->hasta_eko,'assets/files/hasta/')->id;
			$data["data"]["hasta_eko"] = $eko;
		}else{
			$data["data"]["hasta_eko"] = $request->eko_file;
		}
		$hasta->data = $data["data"];
		$hasta->save();
		return back()->with('success','Hasta başarıyla güncellendi!');
	}
	public function search(Request $request){
		$query = $request->get('query','');
		if($request->user()->email == "dreyupozkan@gmail.com"){
			$hws = Hasta::where('tckimlik','LIKE','%'.$query.'%')->get();
		}else{
			$hws = Hasta::where('tckimlik','LIKE','%'.$query.'%')->where('user',$request->user()->id)->get();
		}
		return response()->json($hws);
	}
	public function delete_process($id){
		$hw = Hasta::find($id);
		$hw->delete();
		return back()->with('success','Başarıyla silindi!');
	}
}
