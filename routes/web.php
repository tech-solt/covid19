<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return redirect('/');
});
  Route::get('/', 'Covid19Controller@list')->name('covid19_add2');
Auth::routes();

 Route::get('/admin/covid19/add', 'Covid19Controller@add')->name('covid19_add');
Route::get('/admin/covid19/list', 'Covid19Controller@list')->name('covid19_list');
Route::get('/admin/covid19/update/{id}', 'Covid19Controller@update')->name('covid19_update');
Route::post('/admin/covid19/add_process', 'Covid19Controller@add_process')->name('covid19_add_process');
Route::post('/admin/covid19/update_process', 'Covid19Controller@update_process')->name('covid19_update_process');
Route::get('/admin/covid19/delete/{id}', 'Covid19Controller@delete_process')->name('covid19_delete_process');

Route::get('/admin/covid19/search', 'Covid19Controller@search')->name('covid19_search');
