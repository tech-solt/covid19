<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHastaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hasta', function (Blueprint $table) {
            $table->id();
			$table->string('ad');
			$table->string('soyad');
			$table->integer('yas');
			$table->string('cinsiyet');
			$table->string('tckimlik');
			$table->json('data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hasta');
    }
}
