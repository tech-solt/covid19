var connector;
function Connector() {
    this.ajax = function(obj,callback){
		$.ajax({
			type: obj.type,
			url: obj.url,
			data: obj.data,
			cache: false,
			timeout: 120000,
			success: function (result,textStatus, xhr) {
					callback(result);
			},
			error:function (xhr, ajaxOptions, thrownError){
    			if(xhr.status==404) {
        			callback(xhr.status);
    			}
    			if(xhr.status==422){
    				callback(xhr.responseJSON);
    			}
			}
		});
    };
    this.ajaxFormData = function(obj,callback){
		$.ajax({
			type: obj.type,
			url: obj.url,
			data: obj.data,
			datatype:obj.datatype,
			cache: false,
			timeout: 120000,
			contentType: false,
            processData: false,
			success: function (result,textStatus, xhr) {
					callback(result);
			},
			error:function (xhr, ajaxOptions, thrownError){
    			if(xhr.status==404) {
        			callback(xhr.status);
    			}
    			if(xhr.status==422){
    				callback(xhr.responseJSON);
    			}
			}
		});
    };
    this.ajaxPapara = function(obj,callback){
		$.ajax({
			type: obj.type,
			url: obj.url,
			data: obj.data,
			cache: false,
			timeout: 120000,
  			headers: {
        		'ApiKey':'hULXobAULb7euaCyibEYZoZYdg/hTkHM8iFlAuga6wZmm1oXuOL3xrt+fqmy6zCMrfwO4SMpEZbVf7DW/q2mkQ=='
 			},
			success: function (result,textStatus, xhr) {
				console.log(xhr,result);
					callback(result);
			},
			error:function (xhr, ajaxOptions, thrownError){
    			if(xhr.status==404) {
        			callback(xhr.status);
    			}
    			if(xhr.status==422){
    				callback(xhr.responseJSON);
    			}
			}
		});
    };
}
$(function () {
    connector = new Connector();
});